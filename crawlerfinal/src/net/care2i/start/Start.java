package net.care2i.start;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.care2i.dbdao.CrawlerDAO;
import net.care2i.dbdto.MemberKeywordCategoryDTO;
import net.care2i.tempdto.AccuracyDTO;
import net.care2i.tempdto.KeywordDTO;
import net.care2i.test.BNDM;
import net.care2i.tree.Binary;
import net.care2i.tree.BinaryTree;
import net.care2i.webcrawler.Crawler;
import net.care2i.webcrawler.Match;

public class Start {
	private static int accuracyDownCheck=1;//정확도가 연속적으로 떨어졌는가 체크
	private static String email="";//크롤링할 email
	private static List<MemberKeywordCategoryDTO> list;//멤버키워드카테고리
	private static CrawlerDAO dao;//db연동 dao
	private static Crawler crawler;//crawler 
	private static Match match;//match
	private static KeywordDTO keywordDTO;//신상정보 데이터
	private static ArrayList<String> searchKeywordStoreList;//검색 키워드 저장값 - 첫번째 세대만 해당
	private static ArrayList<String> searchKeywordStoreListSecond;//검색 키워드 저장값2 -두번째세대부터 해당 
	private static HashMap<String, Double> searchKeyword;//검색키워드
	private static ArrayList<AccuracyDTO> accuracyDto;//트리로 저장할 객체
	private static double currentAccuracy=1.0;//현재 정확도
	private static BNDM bndm;//문자열 검색 알고리즘
	private static Binary binary;//최종저장할 이진트리, 정확도,링크,키워드로 구성됨
	private static BinaryTree binaryTree;//이진트리 기능
	private static ArrayList<String> memberInformation;//멤버정보
	private static boolean check_start;//처음시작인지 체크
	private static int count=0;//리소스 누출 전체 카운트
	private static boolean check_mutation=false;//키워드 변이했는지 체크
	
	public Start(String email){
		this.email=email;//이메일초기화
		list=new ArrayList<MemberKeywordCategoryDTO>();//이메일로부터 갖고올 신상정보
		dao=new CrawlerDAO();//database access 부분
		crawler = new Crawler();//크롤러 초기화
		match = new Match();//문자열관련 클래스 초기화
		keywordDTO = new KeywordDTO();//
		searchKeywordStoreList = new ArrayList<String>();
		searchKeywordStoreListSecond = new ArrayList<String>();
		searchKeywordStoreListSecond.add("");
		searchKeyword = new HashMap<String, Double>();
		accuracyDto = new ArrayList<AccuracyDTO>();//트리로 저장할 객체 초기화
		bndm = new BNDM();//믄자열 검색 초기화
		binary = new Binary();//이진트리 초기화
		binaryTree  = new BinaryTree();//이진트리 기능초기화
		memberInformation = new ArrayList<String>();
		check_start=false;//처음일때는 false, 두번째부터는 true
		
		memberInformation=dao.getInformationToList(email);//해당 email 멤버정보를 가져옴

	}
	
	//조합
	public void combination(){
		if(check_start == false){//첫번째부터
			searchKeywordStoreList=match.getcombination(memberInformation);//맨처음 신상정보를 가지고 조합
		}else{//두번째부터
			if(searchKeywordStoreListSecond.get(0) == ""){//키워드조합할것이 없을때
				accuracyDownCheck=10;
			}
			else{//키워드조합할것이있다면
				searchKeywordStoreListSecond=match.getcombination(searchKeywordStoreListSecond);
			}
		}
	}
	//열등한 개체 제거
	public void inferior(){
		
		if(check_start ==false){//처음일때
			accuracyDto=match.judgeInferior(searchKeywordStoreList);
		}else{//두번째부터
			accuracyDto=match.judgeInferior(searchKeywordStoreListSecond,accuracyDto);
		}
		
		//이진트리에저장
		for(int i=0; i<accuracyDto.size() ; i++){
			binary.setAccuracy(accuracyDto.get(i).getAccuracy());
			binary.setKeyword(accuracyDto.get(i).getKeyword());
			binary.setLink(accuracyDto.get(i).getUrl());
			binaryTree.insert(binary);
			System.out.println("키워드 : "+accuracyDto.get(i).getKeyword());
			System.out.println("정확도: "+accuracyDto.get(i).getAccuracy());
			System.out.println("url : "+accuracyDto.get(i).getUrl());
		}
	}
	//우수한 개체판별
	public void superior() throws UnknownHostException{
		for(int i=0; i<accuracyDto.size() ; i++){
			if(!accuracyDto.get(i).getUrl().contains("href") && match.containsHangul(accuracyDto.get(i).getUrl()) ==false ){
				System.out.println("check geturl : "+accuracyDto.get(i).getUrl());
				String scratch=crawler.ScratchManage(accuracyDto.get(i).getUrl());
				for(int listNum=0; listNum<memberInformation.size(); listNum++){
					//신상정보와 유사한가? 맞다면 정확도를 2배해주고 트리에 저장한다.
					if(bndm.findAll(memberInformation.get(listNum), scratch) == true){
						binary.setAccuracy(accuracyDto.get(i).getAccuracy()*2);
						binary.setKeyword(accuracyDto.get(i).getKeyword());
						binary.setLink(accuracyDto.get(i).getUrl());
						binaryTree.insert(binary);
						
						System.out.println("match !");
						System.out.println("찾아라 url : "+accuracyDto.get(i).getUrl());
						System.out.println("찾아라 키워드 : "+accuracyDto.get(i).getKeyword());
						System.out.println("찾아라 정확도 : "+accuracyDto.get(i).getAccuracy());
						System.out.println("------------");
						binaryTree.print();
						System.out.println("------------");
					}
					else{
						System.out.println("no match !");
					}
				}
			}
			
		}	
		
		
	}
	//돌연변이
	public void mutation(){
		if(check_mutation ==false){
			List<String> tempList = new ArrayList<String>();
			List<String> addTempList = new ArrayList<String>();
			tempList=memberInformation;
			
			for(int i=0; i<tempList.size(); i++){
				addTempList=match.converterToMutation(tempList.get(i));
				for(int j=0; j<addTempList.size(); j++){
					searchKeywordStoreListSecond.add(addTempList.get(i));
				}
			}
			check_mutation=true;
		}else{
			accuracyDownCheck=10;
		}
		
	}
	//메인메소드
	public ArrayList<HashMap<String, Double>> start() throws UnknownHostException{
		while(accuracyDownCheck<10){
			combination();
			inferior();
			superior();
			mutation();
			check_start=true;//처음 세대가 끝난다는것을 말함
			//현재정확도가 과거정확도보다 작거나 같다면
			if(accuracyDto.get(accuracyDto.size()-1).getAccuracy() <=currentAccuracy){
				accuracyDownCheck++;
			}else{
				currentAccuracy=accuracyDto.get(accuracyDto.size()-1).getAccuracy();
			}
		}
		
		return null;
	}

}
