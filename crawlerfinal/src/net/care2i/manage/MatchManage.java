package net.care2i.manage;

import java.lang.Character.UnicodeBlock;
import java.util.ArrayList;
import java.util.HashMap;

import com.jaunt.JauntException;

/**
 * <pre>
 * 검색한결과와 비교하는 인터페이스
 * </pre>
 * @author ilovejsp
 * 
 */
public interface MatchManage extends Manage {
	/**
	 * <pre>
	 * 리소스관리하는 메소드
	 * </pre>
	 * @param name 관리객체
	 * @return T/F판단 결과값
	 */
	abstract public boolean ManageMatchResource(Object name); 
	/**
	 * <pre>
	 * 조합칸에서 검색키워드 초기화하는 역활하는 메소드
	 * </pre>
	 * @param inputlist 초기화할 list값들
	 * @return 초기화한후에 <키워드,정확도>값 
	 */
	abstract public HashMap<String, Double> InitSearchKeyword(ArrayList<String> inputlist);
	/**
	 * <pre>
	 * 한글이 있는지 검색
	 * </pre>
	 * @param str 문자열
	 * @return 한글이 포함되있다면 trur 그렇지 안핟면 false
	 */
	public boolean containsHangul(String str);
	abstract public ArrayList<String> Scraper(String url,String keyword,String keyword2) throws JauntException;
	
}
