package net.care2i.manage;
/**
 * <pre>
 * 주기적으로 업데이트 유무 확인 인터페이스
 * </pre>
 * @author ilovejsp
 */
public interface UpdateManage extends Manage{
	/**
	 * <pre>
	 * 주기적으로 업데이트 유무 확인 메소드
	 * </pre>
	 * @param name 관리객체
	 * @return 관리가 제대로된지 T/F 판단 값
	 */
	abstract public boolean ManageUpdateManage(Object name); 
}
