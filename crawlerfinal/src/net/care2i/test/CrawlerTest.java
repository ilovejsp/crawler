package net.care2i.test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.care2i.webcrawler.Crawler;
/**
 * <pre>
 * zum검색엔진에서 링크뽑아오는 것 테스트 클래스
 * </pre>
 * @author ilovejsp
 *
 */
public class CrawlerTest {
	public static void main(String[] args) {
		Crawler crawler = new Crawler();
		String data=crawler.SearchManage("ilovejsp ilovejsp@naver.com");
		
		Pattern pattern = Pattern.compile("href=\"(.*?)\"");
		Matcher matcher = pattern.matcher(data);
		
		while(matcher.find()){
			String result=matcher.group(0).substring(6, matcher.group().length()-1);
			if((!result.startsWith("#")) && result.startsWith("http://") &&(!result.contains("zum"))){
				System.out.println(result);
			}
			
		}
		
	}

}
