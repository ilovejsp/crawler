package net.care2i.test;

import net.care2i.tree.Binary;
import net.care2i.tree.BinaryTree;


public class BinaryTest {

	public static void main(String[] args) {
		Binary binary1 = new Binary();
		binary1.setAccuracy(0.1);
		binary1.setKeyword("testkeyword1");
		binary1.setLeft(null);
		binary1.setRight(null);
		binary1.setLink("http://www.naver.com");

		Binary binary2 = new Binary();
		binary2.setAccuracy(0.001);
		binary2.setKeyword("testkeyword2");
		binary2.setLeft(null);
		binary2.setRight(null);
		binary2.setLink("http://www.daum.com");
		
		Binary binary3 = new Binary();
		binary3.setAccuracy(103.0);
		binary3.setKeyword("testkeyword3");
		binary3.setLeft(null);
		binary3.setRight(null);
		binary3.setLink("http://www.zum.com");

		BinaryTree tree = new BinaryTree();
		Binary binary = new Binary();
		Binary returnValue = new Binary();
		tree.insert(binary1);
		tree.insert(binary2);
		tree.insert(binary3);
		tree.print();
		returnValue = tree.selectAll();
		System.out.println(returnValue.getKeyword());
		System.out.println(returnValue.getLink());
		System.out.println(returnValue.getAccuracy());

		
	}

}
