package net.care2i.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import net.care2i.tempdto.AccuracyDTO;

import org.junit.Test;

public class BinaryListStoreTest {

	@Test
	public void test() {
		List<AccuracyDTO> list = new ArrayList<AccuracyDTO>();
		AccuracyDTO dto1 = new AccuracyDTO();
		dto1.setAccuracy(0.1);
		dto1.setKeyword("key1");
		dto1.setUrl("www.naver.com");
		
		AccuracyDTO dto2 = new AccuracyDTO();
		dto2.setAccuracy(0.2);
		dto2.setKeyword("key2");
		dto2.setUrl("www.daum.com");

		AccuracyDTO dto3 = new AccuracyDTO();
		dto3.setAccuracy(0.3);
		dto3.setKeyword("key3");
		dto3.setUrl("www.yahoo.com");
	
		list.add(dto1);
		list.add(dto2);
		list.add(dto3);
		
		assertEquals("www.yahoo.com",list.get(2).getUrl());
	}

}
