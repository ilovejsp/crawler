package net.care2i.test;

import java.util.ArrayList;
import java.util.List;

public class BNDM {

	
	public boolean findAll(String pattern, String source) {
		boolean flag=false;
		char[] x = pattern.toCharArray();
		char [] y = source.toCharArray();
		int i, j, s, d, last, m = x.length, n = y.length;
		List<Integer> result = new ArrayList<Integer>();
		
		int[] b = new int[65536];

		/* Pre processing */
		for (i = 0; i < b.length; i++)
			b[i] = 0;
		s = 1;
		for (i = m - 1; i >= 0; i--) {
			b[x[i]] |= s;
			s <<= 1;
		}

		/* Searching phase */
		j = 0;
		while (j <= n - m) {
			i = m - 1;
			last = m;
			d = ~0;
			while (i >= 0 && d != 0) {
				d &= b[y[j + i]];
				i--;
				if (d != 0) {
					if (i >= 0)
						last = i + 1;
					else
						flag=true;
						//result.add(j);
				}
				d <<= 1;
			}
			j += last;
		}

		
		return flag;
	}
	/*
	public static List<Integer> findAll(String pattern, String source) {
		char[] x = pattern.toCharArray();
		char [] y = source.toCharArray();
		int i, j, s, d, last, m = x.length, n = y.length;
		List<Integer> result = new ArrayList<Integer>();
		
		int[] b = new int[65536];

		for (i = 0; i < b.length; i++)
			b[i] = 0;
		s = 1;
		for (i = m - 1; i >= 0; i--) {
			b[x[i]] |= s;
			s <<= 1;
		}

		j = 0;
		while (j <= n - m) {
			i = m - 1;
			last = m;
			d = ~0;
			while (i >= 0 && d != 0) {
				d &= b[y[j + i]];
				i--;
				if (d != 0) {
					if (i >= 0)
						last = i + 1;
					else
						result.add(j);
				}
				d <<= 1;
			}
			j += last;
		}

		
		return result;
	}
	*/
		public static void main(String []args){
			BNDM bndm = new BNDM();
		String source="한글 검색 테스트";
		String pattern="검색";
		boolean flag= false;
		flag=bndm.findAll(pattern, source);
		System.out.println(flag);
		/*
		List result = new ArrayList<>();
		result=findAll(pattern, source);
		
		for(int i=0; i<result.size() ; i++){
			System.out.println(result.get(i));
		}
		*/
		
	}
	
}