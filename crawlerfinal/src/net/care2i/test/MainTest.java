package net.care2i.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.care2i.dbdao.CrawlerDAO;
import net.care2i.dbdto.MemberKeywordCategoryDTO;
import net.care2i.tempdto.AccuracyDTO;
import net.care2i.tempdto.KeywordDTO;
import net.care2i.tree.Binary;
import net.care2i.tree.BinaryTree;
import net.care2i.webcrawler.Crawler;
import net.care2i.webcrawler.Match;

/**
 * <pre>
 * 전체 알고리즘 테스트
 * </pre>
 * @author ilovejsp
 *
 */
public class MainTest {
	public static void main(String[] args){
		CrawlerDAO dao = new CrawlerDAO();
		BNDM bndm = new BNDM();
		ArrayList<String> returnList = new ArrayList<String>();
		ArrayList<AccuracyDTO> accuracyDto = new ArrayList<AccuracyDTO>();
		Match match = new Match();
		Crawler crawler = new Crawler();
		Binary binary;//최종저장할 이진트리, 정확도,링크,키워드로 구성됨
		
		BinaryTree binaryTree = new BinaryTree();
		HashMap<String, Double> searchKeyword =new HashMap<String, Double>();
		KeywordDTO keywordDTO= new KeywordDTO();
		List<MemberKeywordCategoryDTO> list = new ArrayList<MemberKeywordCategoryDTO>();
		
		list=dao.getInformation(1);//1번 멤버정보를 갖고옴
		keywordDTO=match.converterToCombinaion(list);//정확도 초기화
		//returnList=match.getcombination(keywordDTO);//갖고온정보를 가지고 조합
		searchKeyword=match.InitSearchKeyword(returnList);//검색키워드 1.0으로초기화
		//accuracyDto=match.ChangeAccuracy(searchKeyword);
		
		
		for(int i=0; i<accuracyDto.size(); i++){
			System.out.println("정확도 : "+accuracyDto.get(i).getAccuracy());
			System.out.println("url : "+accuracyDto.get(i).getUrl());
			System.out.println("키워드 : "+accuracyDto.get(i).getKeyword());
		}
		
		
		//트리형태로 저장
		for(int i=0; i<accuracyDto.size() ; i++){
			System.out.println(accuracyDto.get(i).getUrl());
			binary = new Binary();
			binary.setAccuracy(accuracyDto.get(i).getAccuracy());
			binary.setKeyword(accuracyDto.get(i).getKeyword());
			binary.setLink(accuracyDto.get(i).getUrl());
			binaryTree.insert(binary);
		}
		binaryTree.print();
		
		//우수한 개체 판별
		for(int i=0; i<accuracyDto.size() ; i++){
			String scratch=crawler.ScratchManage(accuracyDto.get(i).getUrl());
			for(int listNum=0; listNum<list.size(); listNum++){
				//신상정보와 유사한가? 맞다면 정확도를 2배해주고 트리에 저장한다.
				if(bndm.findAll(list.get(listNum).getContent(), scratch) == true){
					binary = new Binary();
					binary.setAccuracy(accuracyDto.get(i).getAccuracy()*2);
					binary.setKeyword(accuracyDto.get(i).getKeyword());
					binary.setLink(accuracyDto.get(i).getUrl());
					binaryTree.insert(binary);
					
					System.out.println("match !");
					System.out.println("찾아라 url : "+accuracyDto.get(i).getUrl());
					System.out.println("찾아라 키워드 : "+accuracyDto.get(i).getKeyword());
					System.out.println("------------");
					binaryTree.print();
					System.out.println("------------");
				}
				else{
					System.out.println("no match !");
				}
			}
			
		}
	}
}
