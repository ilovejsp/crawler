package net.care2i.test;

import java.lang.Character.UnicodeBlock;

public class HangulContain {

	public static void main(String[] args) {
		String input="abc";
		System.out.println(HangulContain.containsHangul(input));
	}

	public static boolean containsHangul(String str) {
		for (int i = 0; i < str.length(); i++) {
			char ch = str.charAt(i);
			Character.UnicodeBlock unicodeBlock = Character.UnicodeBlock.of(ch);
			if (UnicodeBlock.HANGUL_SYLLABLES.equals(unicodeBlock)
				|| UnicodeBlock.HANGUL_COMPATIBILITY_JAMO.equals(unicodeBlock) 
				|| UnicodeBlock.HANGUL_JAMO.equals(unicodeBlock))
				return true;
		}
		return false;
	}
}
