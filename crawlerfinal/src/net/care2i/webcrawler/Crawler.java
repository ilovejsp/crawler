package net.care2i.webcrawler;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import net.care2i.manage.CollectorManage;
import net.care2i.manage.ResourceManage;
import net.care2i.manage.UpdateManage;
import net.care2i.tempdto.SearchKeyword;
/**
 * <pre>
 * 웹크롤러 클래스
 * </pre>
 * @author ilovejsp
 */
public class Crawler implements CollectorManage,ResourceManage,UpdateManage{
	@Override
	public boolean ManageResource(Object name) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean ManageUpdateManage(Object name) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean ManageResourceManage(Object name) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean ManageCollectorResource(Object name) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public SearchKeyword SearchManage(SearchKeyword searchkey) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String SearchManage(String key) {
		key=key.replace(" ", "+");
		HttpClient httpClient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(
				"http://search.zum.com/search.zum?method=uni&option=accu&qm=f_typing.top&query="+key);
		StringBuilder content = new StringBuilder();

		try {
			HttpResponse response = httpClient.execute(httpGet);

			int bufferLength = 1024;
			byte[] buffer = new byte[bufferLength];
			InputStream is = response.getEntity().getContent();

			while (is.read(buffer) != -1) {
				content.append(new String(buffer, "UTF-8"));
			}

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return content.toString();
	}
	@Override
	public String ScratchManage(String url) {
		HttpClient httpClient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(url);
		StringBuilder content = new StringBuilder();

		try {
			HttpResponse response = httpClient.execute(httpGet);

			int bufferLength = 1024;
			byte[] buffer = new byte[bufferLength];
			InputStream is = response.getEntity().getContent();

			while (is.read(buffer) != -1) {
				content.append(new String(buffer, "UTF-8"));
			}

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return content.toString();
	}

}
