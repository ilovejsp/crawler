package net.care2i.dbdto;
/**
 * 
 * @author ilovejsp
 */
public class MemberDTO {
	private int no;
	private String email;
	private String password;
	/**
	 * <pre>
	 * MemberDTO생성자
	 * </pre>
	 */
	public MemberDTO(){
		super();
	}
	/**
	 * <pre>
	 * MemberDTO생성자
	 * </pre>
	 */	
	public MemberDTO(int no,String email,String password){
		super();
		this.no=no;
		this.email=email;
		this.password=password;
	}
	/**
	 * 
	 * @return memberkey
	 */
	public int getNo() {
		return no;
	}
	/**
	 * 
	 * @param no memberkey
	 */
	public void setNo(int no) {
		this.no = no;
	}
	/**
	 * 
	 * @return 회원 이메일
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * 
	 * @param email 회원이메일
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * 
	 * @return 비밀번호
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * 
	 * @param password 비밀번호
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
}
