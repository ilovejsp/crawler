package net.care2i.dbdto;

/**
 * 
 * @author ilovejsp
 * 멤버키워드카테고리 DB
 */
public class MemberKeywordCategoryDTO {

	private int no;
	private String content;
	private String type;
	/**
	 * 
	 * @return 멤버키워드카테고리 key
	 */
	public int getNo() {
		return no;
	}
	/**
	 * 
	 * @param no 멤버키워드카테고리 key
	 */
	public void setNo(int no) {
		this.no = no;
	}
	/**
	 * 
	 * @return 멤버키워드카테고리 내용
	 */
	public String getContent() {
		return content;
	}
	/**
	 * 
	 * @param content 멤버키워드카테고리내용
	 */
	public void setContent(String content) {
		this.content = content;
	}
	/**
	 * 
	 * @return 멤버키워드카테고리 키워드 타입
	 */
	public String getType() {
		return type;
	}
	/**
	 * 
	 * @param type 멤버키워드카테고리 키워드 타입
	 */
	public void setType(String type) {
		this.type = type;
	}
}
