package net.care2i.tempdto;

import java.util.ArrayList;
import java.util.HashMap;
/**
 * <pre>
 * 신상정보 Data
 * </pre>
 * @author ilovejsp
 */
public class KeywordDTO {
	private HashMap<String, Double> id = new HashMap<String, Double>();
	private HashMap<String, Double> name = new HashMap<String, Double>();
	private HashMap<String, Double> address = new HashMap<String, Double>();
	private HashMap<String, Double> univschool = new HashMap<String, Double>();
	private HashMap<String, Double> phone = new HashMap<String, Double>();
	private HashMap<String, Double> birth = new HashMap<String, Double>();
	private HashMap<String, Double> email = new HashMap<String, Double>();
	private ArrayList<HashMap<String, Double>> list = new ArrayList<HashMap<String,Double>>();
	/**
	 * 
	 * @return 신상정보 타입중 이메일
	 */
	public HashMap<String, Double> getEmail() {
		return email;
	}
	/**
	 * 
	 * @param email 신상정보 타입중 이메일
	 */
	public void setEmail(HashMap<String, Double> email) {
		this.email = email;
	}
	/**
	 * 
	 * @return 신상정보 타입중 id
	 */
	public HashMap<String, Double> getId() {
		return id;
	}
	/**
	 * 
	 * @param id 신상정보 타입중 id
	 */
	public void setId(HashMap<String, Double> id) {
		this.id = id;
	}
	/**
	 * 
	 * @return 신상정보 타입중 이름
	 */
	public HashMap<String, Double> getName() {
		return name;
	}
	/**
	 * 
	 * @param name 신상정보 타입중 이름
	 */
	public void setName(HashMap<String, Double> name) {
		this.name = name;
	}
	/**
	 * 
	 * @return 신상정보 타입중 주소
	 */
	public HashMap<String, Double> getAddress() {
		return address;
	}
	/**
	 * 
	 * @param address 신상정보 타입중 주소
	 */
	public void setAddress(HashMap<String, Double> address) {
		this.address = address;
	}
	/**
	 * 
	 * @return 신상정보 타입중 학교
	 */
	public HashMap<String, Double> getUnivschool() {
		return univschool;
	}
	/**
	 * 
	 * @param univschool 신상정보 타입중 학교
	 */
	public void setUnivschool(HashMap<String, Double> univschool) {
		this.univschool = univschool;
	}
	/**
	 * 
	 * @return 신상정보 타입중 번호
	 */
	public HashMap<String, Double> getPhone() {
		return phone;
	}
	/**
	 * 
	 * @param phone 신상정보 타입중 번호
	 */
	public void setPhone(HashMap<String, Double> phone) {
		this.phone = phone;
	}
	/**
	 * 
	 * @return 신상정보 타입중 생일
	 */
	public HashMap<String, Double> getBirth() {
		return birth;
	}
	/**
	 * 
	 * @param birth 신상정보 타입중 생일
	 */
	public void setBirth(HashMap<String, Double> birth) {
		this.birth = birth;
	}
	/**
	 * 
	 * @return keywordDto를 list받은객체
	 */
	public ArrayList<HashMap<String, Double>> getList() {
		return list;
	}
	/**
	 * 
	 * @param list keywordDto를 list받은객체
	 */
	public void setList(ArrayList<HashMap<String, Double>> list) {
		this.list = list;
	}
	
	
}
