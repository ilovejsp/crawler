package net.care2i.tempdto;
/**
 * <pre>
 * 레드블랙트리 DTO
 * </pre>
 * @author ilovejsp
 * @version 1.0
 */
public class AccuracyDTO {
	private String url;
	private double accuracy;
	private String keyword;
	/**
	 * 
	 * @return 키워드값들
	 */
	public String getKeyword() {
		return keyword;
	}
	/**
	 * 
	 * @param keyword 키워드값들
	 */
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	/**
	 * 
	 * @return 링크
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * 
	 * @param url 링크
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	/**
	 * 
	 * @return 정확도
	 */
	public double getAccuracy() {
		return accuracy;
	}
	/**
	 * 
	 * @param accuracy 정확도
	 */
	public void setAccuracy(double accuracy) {
		this.accuracy = accuracy;
	}
	
}
