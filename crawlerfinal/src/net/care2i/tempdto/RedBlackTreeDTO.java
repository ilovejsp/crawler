package net.care2i.tempdto;
/**
 * <pre>
 * 레드블랙트리클래스
 * </pre>
 * @version 2.0
 * @author user
 *
 */
public class RedBlackTreeDTO {
	private static final boolean RED=true;
	private static final boolean BLACK=false;
	private RedBlackTree tree;
	private class RedBlackTree{
		private double accuracy;//정확도
		private String keyword;//키워드
		private String link;//링크
		private boolean color;//색깔
		private RedBlackTree left;//왼쪽자식노드
		private RedBlackTree right;//오른쪽자식노드
		/**
		 * 
		 * @return 레드블랙트리 정확도
		 */
		public double getAccuracy() {
			return accuracy;
		}
		/**
		 * 
		 * @param accuracy 레드블랙트리 정확도
		 */
		public void setAccuracy(double accuracy) {
			this.accuracy = accuracy;
		}
		/**
		 * 
		 * @return 레드블랙트리 키워드
		 */
		public String getKeyword() {
			return keyword;
		}
		/**
		 * 
		 * @param keyword 레드블랙트리 키워드
		 */
		public void setKeyword(String keyword) {
			this.keyword = keyword;
		}
		/**
		 * 
		 * @return 레드블랙트리 링크
		 */
		public String getLink() {
			return link;
		}
		/**
		 * 
		 * @param link 레드블랙트리 링크
		 */
		public void setLink(String link) {
			this.link = link;
		}
		/**
		 * 
		 * @return 레드블랙트리 색깔
		 */
		public boolean isColor() {
			return color;
		}
		/**
		 * 
		 * @param color 레드블랙트리 색깔
		 */
		public void setColor(boolean color) {
			this.color = color;
		}
		/**
		 * 
		 * @return 레드블랙트리 왼쪽자식
		 */
		public RedBlackTree getLeft() {
			return left;
		}
		/**
		 * 
		 * @param left 레드블랙트리 왼쪽자식
		 */
		public void setLeft(RedBlackTree left) {
			this.left = left;
		}
		/**
		 * 
		 * @return 레드블랙트리 오른쪽자식
		 */
		public RedBlackTree getRight() {
			return right;
		}
		/**
		 * 
		 * @param right 레드블랙트리 오른쪽자식
		 */
		public void setRight(RedBlackTree right) {
			this.right = right;
		}
		
		
	}
}
