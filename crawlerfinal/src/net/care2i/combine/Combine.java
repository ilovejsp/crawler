package net.care2i.combine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.care2i.dbdto.MemberKeywordCategoryDTO;
import net.care2i.tempdto.KeywordDTO;

/**
 * <pre>
 * 키워드 조합하는 인터페이스
 * </pre>
 * @author ilovejsp
 */
public interface Combine {
	public void combination(int size,ArrayList<String> list);
	public void print(int size,ArrayList<String> list,int from,final int[] selected);
	public ArrayList<String> getcombination(ArrayList<String> list);
	/*
	public void combination(int size,ArrayList<HashMap<String, Double>> arr);
	public void print(int size,ArrayList<HashMap<String, Double>> arr,int from,final int[] selected);
	public ArrayList<String> getcombination(KeywordDTO keyworddto);
	*/
	public KeywordDTO converterToCombinaion(List<MemberKeywordCategoryDTO> inputList);
	
	
	/**
	 * <pre>
	 * 키워드 변경 알고리즘(1.숫자변경,2.주소 자르기,3.전화번호변경,4.이메일변경)
	 * 현재는 1번만 적용
	 * 문자로만 이루어진 경우->아무것도안하고
	 * 숫자를 포함할경우->+1,-1을 해준다.	
	 *
	 * </pre>
	 * @param keyword 변경하고 싶은 키워드
	 * @return 변경된 키워드들
	 */
	public ArrayList<String> converterToMutation(String keyword);
	/**
	 * <pre>
	 * 대괄호를 string값으로 바꿔주는 키워드
	 * </pre>
	 * @param input 바꿀 문자열
	 * @return 대괄호를 제외한 문자열
	 */
	public String ConverterToString(String input);
}
